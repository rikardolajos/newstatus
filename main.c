#define _XOPEN_SOURCE

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

const size_t max_buffer = 1024;

int print_userhost(char *buffer)
{
    FILE *fu = popen("whoami", "r");
    FILE *fh = popen("hostname", "r");

    size_t size = 128;
    char user[size];
    char host[size];

    fgets(user, size, fu);	
    fgets(host, size, fh);

    pclose(fu);
    pclose(fh);

    /* Replace newline with NULL-byte. */
    user[strlen(user) - 1] = '\0';
    host[strlen(host) - 1] = '\0';

    return sprintf(buffer, "%s@%s", user, host);
}

int print_cpuload(char *buffer)
{
    FILE *f = popen("cat /proc/loadavg", "r");
    
    float avg;
    fscanf(f, "%f", &avg);
    pclose(f);

    return sprintf(buffer, "CPU: %03.0f load", avg * 100);
}

int print_ramfree(char *buffer)
{
    FILE *f = popen("free -h | head -n 2 | tail -n 1 | awk '{print $4}'", "r");
    
    char free[16] = {0};
    fscanf(f, "%s", free);
    pclose(f);

    return sprintf(buffer, "RAM: %s free", free);
}

int print_ipaddress(char *buffer)
{
    FILE *f = popen("ip -o -4 addr show dev enp2s0"
                    "| cut -d ' ' -f 7 | cut -f 1 -d '/'",
                    "r");
    
    size_t size = 128;
    char s[size];
    
    fgets(s, size, f);
    pclose(f);

    size_t length = strlen(s);
    
    if (length < 3) {
        return sprintf(buffer, "enp2s0: error reading ip");
    }

    /* Replace the newline with NULL-byte. */
    s[length - 1] = '\0';

    return sprintf(buffer, "enp2s0: %s", s);
}

int print_datetime(char *buffer)
{
    time_t raw_time;
    struct tm *current_time;
    
    time(&raw_time);
    current_time = localtime(&raw_time);

    return strftime(buffer, max_buffer, "%Y-%m-%d %A w.%V %H:%M:%S",
                    current_time);
}

int print_info(char *buffer, char *info)
{
    return sprintf(buffer, "%s", info);
}

int print_separator(char *buffer)
{
    return sprintf(buffer, " | ");
}

int print_newline(char *buffer)
{
    return sprintf(buffer, "\n");
}

int print_volume(char *buffer)
{
    FILE *f = popen("pamixer --get-volume", "r");
    
    int volume;
    fscanf(f, "%d", &volume);
    pclose(f);

    f = popen("pamixer --get-mute", "r");
    char mute[16] = {0};
    fscanf(f, "%s", mute);
    pclose(f);

    if (!strcmp(mute, "true")) {
        return sprintf(buffer, "Vol: --- %%");
    }

    return sprintf(buffer, "Vol: %03d %%", volume);
}

int main(int argc, char *argv[])
{
    int opt;
    unsigned int sleep_time = 1;

    while ((opt = getopt(argc, argv, "s:")) != -1) {
        switch (opt) {
        case 's':
            sleep_time = atoi(optarg);
            break;
        default:
            fprintf(stderr, "Usage: %s [-s sleep_time]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }
    
    char buffer[max_buffer];
    
    while (true) {
        int position = 0;
        memset(buffer, 0, max_buffer);
        
        position += print_userhost(&buffer[position]);
        position += print_separator(&buffer[position]);
        position += print_volume(&buffer[position]);
        position += print_separator(&buffer[position]);
        position += print_cpuload(&buffer[position]);
        position += print_separator(&buffer[position]);
        position += print_ramfree(&buffer[position]);
        position += print_separator(&buffer[position]);
        position += print_ipaddress(&buffer[position]);
        position += print_separator(&buffer[position]);
        position += print_datetime(&buffer[position]);
        position += print_newline(&buffer[position]);

        fprintf(stdout, buffer);
        fflush(stdout);
        sleep(sleep_time);
    }

    exit(EXIT_SUCCESS);
}

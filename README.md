Information
==========
This is a replacement program for i3status.

Compile and Install
==================
To compile `newstatus`, simply run:
```
make
```

To install `newstatus`, again simply run:
```
sudo make install
```
This will install the program into `/usr/local/bin/`.

Setting up i3
=============
Your i3 config file `~.i3\config` must be altered to use `newstatus` instead of `i3status`. This is done by changing `status_command` in the `bar { ... }` section. The resulting section should look something like this:
```
bar {
    position top
    status_command newstatus
}
```

Launch Options
==============
`newstatus` can take one optional argument at launch, `newstatus -s 5`, to set the sleep time between refreshes. This example will set the sleep time to at least 5 seconds. Default is 1 second. 

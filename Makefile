all:
	gcc -o newstatus main.c -std=c11 -Wall -Werror -O2 -pedantic

install: all
	mv ./newstatus /usr/local/bin/
